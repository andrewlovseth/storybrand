	<footer>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="copyright">
				<div class="notice">
					<p><?php the_field('copyright', 'options'); ?></p>
				</div>

				<div class="contact">
					<p>
						<span class="phone"><?php the_field('phone', 'options'); ?></span> &middot; 
						<a href="<?php the_field('terms_link', 'options'); ?>" rel="external"><?php the_field('terms_link_label', 'options'); ?></a>
					</p>
				</div>
				
			</div>

			<div class="footer-nav">
				<?php if(have_rows('footer_nav', 'options')): while(have_rows('footer_nav', 'options')): the_row(); ?>

					<div class="link">
						<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_label'); ?></a>						
					</div>
		
				<?php endwhile; endif; ?>
			</div>

		</div>
	</footer>

	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js?v=1.0.1"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js?v=1.0.2"></script>
	
	<?php wp_footer(); ?>

	<?php the_field('footer_scripts', 'options'); ?>
</body>
</html>