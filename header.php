<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://use.typekit.net/eep5iue.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>?v=1.0.15" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<?php wp_head(); ?>

	<?php the_field('header_scripts', 'options'); ?>
</head>

<body <?php body_class(); ?> id="<?php echo $post->post_name; ?>">

	<div class="nav-bg"></div>

	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<a href="#" id="toggle">
				<div class="patty"></div>
			</a>		

			<nav>
				<?php if(have_rows('header_nav', 'options')): while(have_rows('header_nav', 'options')): the_row(); ?>

					<?php if(get_sub_field('button') == true): ?>

						<?php if(have_rows('sections')): ?>

							<?php while(have_rows('sections')) : the_row(); ?>
	 
							    <?php if( get_row_layout() == 'header_anchor' ): ?>
									
									<?php get_template_part('partials/landing-page/header-anchor'); ?>
									
							    <?php endif; ?>
							 
							<?php endwhile; ?>

						<?php else: ?>	

						    <a href="<?php the_sub_field('link'); ?>" class="btn small"><?php the_sub_field('link_label'); ?></a>

						<?php endif; ?>

					<?php else: ?>

					    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_label'); ?></a>

					<?php endif; ?>

				<?php endwhile; endif; ?>
			</nav>


		</div>
	</header>