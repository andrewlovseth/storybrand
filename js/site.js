(function($){  
	
	$(document).ready(function() {
	
		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});
	
		// Menu Toggle
		$('#toggle').click(function(){
			$('header, nav, .nav-bg').toggleClass('open');
			return false;
		});
	
		// Smoothscroll	
		$('a.btn, #showcase-links a, #tpbr_calltoaction').smoothScroll();
	
		$('#difference a').on('click', function(){
	
			var more = $('#difference .more'),
				moreText = $(this).data('more-text'),
				lessText = $(this).data('less-text'),
				text = $(more).is(':visible') ? moreText : lessText;
	
	     	$(this).text(text);
	
			$(more).slideToggle(400);
	
	
			return false;
		});
	
	
		// Close Overlay, Remove iframe src
		$('.video-trigger').on('click', function(){
	
			$('#overlay').css('display', 'table');
	
			var videoSrc = $(this).data('src');
	
			var iframe = $('#overlay').find('iframe');
			iframe.attr('src', videoSrc);
	
			return false;
		});
	
	
		// Close Overlay, Remove iframe src
		$('#overlay .overlay-x').on('click', function(){
	
			$('#overlay').hide();
	
			var iframe = $('#overlay').find('iframe');
			iframe.attr('src','');
	
			return false;
		});
	
	
	
		// Home Hero Carousel
		$('section#testimonials').slick({
			dots: false,
			arrows: false,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
		    fade: true,
		    cssEase: 'linear',
	
		});

		// Landing Page Testimonials Carousel
		$('section.testimonials').slick({
			dots: false,
			arrows: false,
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
		    fade: true,
		    cssEase: 'linear',
	
		});
	
	
		$('a[data-slide]').click(function(e) {
			e.preventDefault();
			var slideTo = $(this).data('slide');
			$('section#testimonials, section.testimonials').slick('slickGoTo', slideTo - 1);
		});
	
	
		// Cover Hero Video
		var timeoutId;
		var $videoBgAspect = $(".videobg-aspect");
		var $videoBgWidth = $(".videobg-width");
		var videoAspect = $videoBgAspect.outerHeight() / $videoBgAspect.outerWidth();
	
		function videobgEnlarge() {
		  windowAspect = ($(window).height() / $(window).width());
		  if (windowAspect > videoAspect) {
		    $videoBgWidth.width((windowAspect / videoAspect) * 100 + '%');
		  } else {
		    $videoBgWidth.width(100 + "%")
		  }
		}
	
		$(window).resize(function() {
		  clearTimeout(timeoutId);
		  timeoutId = setTimeout(videobgEnlarge, 100);
		});
	
		$(function() {
		  videobgEnlarge();
		});
	
	
			
	});

})(jQuery); 
