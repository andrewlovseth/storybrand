<?php get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);">
		
		<div class="video-box">

			<div class="videobg">
			  <div class="videobg-width">
			    <div class="videobg-aspect">
			      <div class="videobg-make-height">
			      	<?php the_field('hero_video_embed'); ?>
			      </div>
			    </div>
			  </div>
			</div>
		
			<script src="https://player.vimeo.com/api/player.js"></script>

		</div>

		<div class="content">
			<div class="wrapper">

				<div class="info">
					<h1><?php the_field('hero_headline'); ?></h1>
					<h2><?php the_field('hero_sub_headline'); ?></h2>

					<?php get_template_part('partials/home/register-btn'); ?>

					<div class="watch-video">
						<a href="#" class="video-trigger" data-src="//player.vimeo.com/video/<?php the_field('hero_video_id'); ?>?title=0&byline=0&portrait=0&autoplay=true"><span>Watch Video</span></a>
					</div>
				</div>			

			</div>
		</div>
	</section>

	<section id="showcase-links">

		<?php if(have_rows('showcase_links')): while(have_rows('showcase_links')): the_row(); ?>
		 
		    <div class="link">
		    	<a href="<?php the_sub_field('link'); ?>">
		    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    		<span class="label"><?php the_sub_field('link_label'); ?></span>
		    	</a>
		    </div>

		<?php endwhile; endif; ?>

	</section>



	<section id="message" class="default-spacing">
		<div class="wrapper">

			<h2 class="section-header"><?php the_field('message_headline'); ?></h2>

			<div class="features">
				<?php if(have_rows('message_features')): while(have_rows('message_features')): the_row(); ?>
				 
				    <div class="feature">
				        <p><?php the_sub_field('feature'); ?></p>
				    </div>

				<?php endwhile; endif; ?>
			</div>

			<?php get_template_part('partials/home/register-btn'); ?>
		
		</div>
	</section>


	<section id="register" class="default-spacing">
		<div class="wrapper">
			
			<h2 class="section-header"><?php the_field('register_headline'); ?></h2>

			<div class="options">
				<?php if(have_rows('register_options')): while(have_rows('register_options')): the_row(); ?>
			 
			    <div class="option <?php the_sub_field('color'); ?>">
			    	<div class="header">
			    		<h3><?php the_sub_field('headline'); ?></h3>
			    	</div>

			    	<div class="image">
			    		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			    	</div>

					<?php if(get_sub_field('show_cost')): ?>

				    	<div class="cost">
				    		<h2><?php the_sub_field('cost'); ?></h2>
				    		<em><?php the_sub_field('cost_note'); ?></em>
				    	</div>

				    <?php endif; ?>

			    	<div class="message">
			    		<p><?php the_sub_field('message'); ?></p>
			    	</div>

			    	<div class="cta">
			    		<a class="btn" href="<?php the_sub_field('cta_link'); ?>"><?php the_sub_field('cta_label'); ?></a>
			    	</div>

			    </div>

				<?php endwhile; endif; ?>

			</div>

			<div class="testimonials">
				
				<?php if(have_rows('register_testimonials')): while(have_rows('register_testimonials')): the_row(); ?>
				 
				    <div class="testimonial">
						<blockquote>
							<p><?php the_sub_field('quote'); ?></p>
						</blockquote>

						<cite>
							<h4><?php the_sub_field('source'); ?></h4>
							<h5><?php the_sub_field('source_note'); ?></h5>
						</cite>
				    </div>

				<?php endwhile; endif; ?>

			</div>

		</div>
	</section>


	<section id="custom-pricing" class="default-spacing">
		<div class="wrapper">
			
			<div class="info">
				<?php the_field('custom_pricing'); ?>
			</div>

		</div>
	</section>


	<section id="who-its-for" class="default-spacing">
		<div class="wrapper">

			<h2 class="section-header"><?php the_field('who_its_for_headline'); ?></h2>

			<div class="company-types">

				<?php if(have_rows('company_types')): while(have_rows('company_types')): the_row(); ?>
	 
				    <div class="company-type">
				        <p><span><?php the_sub_field('company_type'); ?></span></p>
				    </div>

				<?php endwhile; endif; ?>

			</div>

			<div class="company-types-note">
				<em><?php the_field('company_types_note'); ?></em>
			</div>
			
			<?php get_template_part('partials/home/register-btn'); ?>

			<?php get_template_part('partials/home/register-note'); ?>


		</div>
	</section>


	<section id="why-storybrand" class="default-spacing">
		<div class="wrapper">

			<div class="testimonial">
				<blockquote>
					<p><?php the_field('testimonial_quote'); ?></p>
				</blockquote>

				<cite>
					<h4><?php the_field('testimonial_source'); ?></h4>
					<h5><?php the_field('testimonial_source_note'); ?></h5>
				</cite>
			</div>

			<div class="features">
				
				<?php if(have_rows('why_storybrand_features')): while(have_rows('why_storybrand_features')): the_row(); ?>
				 
				    <div class="feature">
				    	<div class="image">
				    		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    	</div>
				    	<div class="info">
				    		<h3><?php the_sub_field('headline'); ?></h3>
				    		<?php the_sub_field('copy'); ?>
				    	</div>        
				    </div>

				<?php endwhile; endif; ?>

			</div>
			
			<?php get_template_part('partials/home/register-btn'); ?>

			<?php get_template_part('partials/home/register-note'); ?>

		</div>
	</section>


	<section class="video cover" style="background-image: url(<?php $image = get_field('video_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">
				
				<div class="info">
					<h3><?php the_field('video_headline'); ?></h3>
					<div class="play">
						<a href="#" class="video-trigger" data-src="//player.vimeo.com/video/<?php the_field('video_id'); ?>?title=0&byline=0&portrait=0&autoplay=true"><span>Watch Video</span></a>
					</div>
				</div>

			</div>
		</div>
	</section>


	<section id="difference" class="default-spacing">
		<div class="wrapper">

			<div class="info">
				<h2 class="section-header"><?php the_field('difference_headline'); ?></h2>
				<?php the_field('difference_copy'); ?>
				<a href="<?php the_field('difference_link'); ?>" data-more-text="<?php the_field('difference_link_label'); ?>" data-less-text="<?php the_field('difference_link_label_less'); ?>"><?php the_field('difference_link_label'); ?></a>

				<div class="more">
					<?php the_field('difference_more_copy'); ?>
				</div>				
			</div>
		

		</div>
	</section>


	<section id="testimonials">

		<?php if(have_rows('testimonials')): while(have_rows('testimonials')): the_row(); ?>
		 
			<div class="slide cover" id="<?php echo sanitize_title_with_dashes(get_sub_field('source')); ?>" style="background-image: url(<?php $image = get_sub_field('background_image'); echo $image['url']; ?>);">
				<div class="content">
					<div class="wrapper">
						
					    <div class="info">
							<blockquote>
								<p><?php the_sub_field('quote'); ?></p>
							</blockquote>

							<cite>
								<h4><?php the_sub_field('source'); ?></h4>
								<h5><?php the_sub_field('source_note'); ?></h5>
							</cite>
					    </div>

					</div>
				</div>
			</div>

		<?php endwhile; endif; ?>


	</section>


	<section id="testimonials-nav">
		
		<?php if(have_rows('testimonials')): $counter = 1; ?>

			<?php while(have_rows('testimonials')): the_row(); ?>

				<a href="#" data-slide="<?php echo $counter; ?>">
					<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

			<?php $counter++; endwhile; ?>

		<?php endif; ?>

	</section>


	<section id="live-workshop"  class="live default-spacing">
		<div class="wrapper">
			
			<h3 class="sub-headline"><?php the_field('live_workshop_subheadline'); ?></h3>
			<h2 class="section-header"><?php the_field('live_workshop_headline'); ?></h2>

			<div class="info">
				<?php the_field('live_workshop_copy'); ?>
			</div>


			<div class="options">
				<?php if(have_rows('live_workshop_options')): while(have_rows('live_workshop_options')): the_row(); ?>
			 
			    <div class="option">
			    	<div class="header">
			    		<h4><?php the_sub_field('sub_headline'); ?></h4>
			    		<h3><?php the_sub_field('headline'); ?></h3>
			    	</div>

			    	<div class="cost">
			    		<h2><?php the_sub_field('cost'); ?></h2>
			    		<em><?php the_sub_field('cost_note'); ?></em>
			    	</div>

			    	<div class="cta">
			    		<a class="btn" href="#live-workshop-register">Register Now</a>
			    	</div>

			    </div>

				<?php endwhile; endif; ?>

			</div>

			<div class="note">
				<?php the_field('live_workshop_note'); ?>
			</div>

		</div>
	</section>


	<section id="live-workshop-calendar" class="live default-spacing">
		<div class="wrapper">

			<h3 class="sub-headline"><?php the_field('live_workshop_subheadline'); ?></h3>
			<h2 class="section-header"><?php the_field('live_workshop_calendar_headline'); ?></h2>

			<div class="events">

				<?php if(have_rows('calendar')): while(have_rows('calendar')): the_row(); ?>

				    <div class="event cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
				    	<div class="content">
					    	
					    	<div class="location">
					    		<h3><?php the_sub_field('city'); ?></h3>
					    		<h4><?php the_sub_field('state'); ?></h4>
					    		
					    	</div>

					    	<div class="info">
					    		<div class="info-wrapper">

					    			<div class="dates">
					    				<?php if(get_sub_field('date_1')): ?>
										    <div class="date">
										        <p><span><?php the_sub_field('date_1'); ?></span></p>
										    </div>
						    			<?php endif; ?>
					    			
					    				<?php if(get_sub_field('date_2')): ?>
										    <div class="date">
										        <p><span><?php the_sub_field('date_2'); ?></span></p>
										    </div>
						    			<?php endif; ?>

					    				<?php if(get_sub_field('date_3')): ?>
										    <div class="date">
										        <p><span><?php the_sub_field('date_3'); ?></span></p>
										    </div>
						    			<?php endif; ?>
					    			</div>

					    			<div class="cta">
					    				<a href="#live-workshop-register" class="btn"><?php the_sub_field('cta_link_label'); ?></a>
					    			</div>
					    			
					    		</div>
					    	</div>				    		

				    	</div>				        
				    </div>

				<?php endwhile; endif; ?>

			</div>

		</div>
	</section>


	<section id="live-workshop-features" class="live default-spacing">
						
		<div class="info">
			<div class="info-wrapper">

				<h3 class="sub-headline"><?php the_field('live_workshop_subheadline'); ?></h3>
				<h2 class="section-header"><?php the_field('live_workshop_features_headline'); ?></h2>

				<div class="features">
					<?php if(have_rows('live_workshop_features')): while(have_rows('live_workshop_features')): the_row(); ?>
					 
					    <div class="feature">
					        <p><span><?php the_sub_field('feature'); ?></span></p>
					    </div>

					<?php endwhile; endif; ?>
				</div>
				
			</div>

		</div>

		<div class="graphic">
			<div class="graphic-wrapper">
				<img src="<?php $image = get_field('live_workshop_features_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />				
			</div>
		</div>

	</section>


	<section id="live-workshop-register" class="live default-spacing">
		<div class="wrapper">

			<div class="form-wrapper">
				
				<div class="form-headline">
					<h2 class="section-header"><?php the_field('live_workshop_register_headline'); ?></h2>
				</div>

				<div class="form-body">
					<?php $shortcode = get_field('live_workshop_register_shortcode'); echo do_shortcode($shortcode); ?>
				</div>

			</div>
			
		</div>
	</section>

	
	<div id="overlay">
		<div>
			<div class="overlay-x"><span class="video-x-circle">X</span> CLOSE VIDEO</div>
			<div class="videoWrapper videoWrapperHide">
				<iframe width="1000" height="562" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
			</div>
		</div>
	</div>

<?php get_footer(); ?>