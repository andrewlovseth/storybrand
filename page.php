<?php get_header(); ?>


    <?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
     
        <?php if( get_row_layout() == 'hero_background_image' ): ?>

        	<?php get_template_part('partials/landing-page/hero-background-image'); ?>

        <?php elseif( get_row_layout() == 'inline_video' ): ?>

            <?php get_template_part('partials/landing-page/inline-video'); ?>

        <?php elseif( get_row_layout() == 'anchor' ): ?>

            <?php get_template_part('partials/landing-page/anchor'); ?>

        <?php elseif( get_row_layout() == 'paragraph' ): ?>

            <?php get_template_part('partials/landing-page/paragraph'); ?>

        <?php elseif( get_row_layout() == 'testimonials' ): ?>

        	<?php get_template_part('partials/landing-page/testimonials'); ?>

        <?php elseif( get_row_layout() == 'calendar' ): ?>

        	<?php get_template_part('partials/landing-page/calendar'); ?>

        <?php elseif( get_row_layout() == 'pricing' ): ?>

            <?php get_template_part('partials/landing-page/pricing'); ?>

        <?php elseif( get_row_layout() == 'register_form' ): ?>

        	<?php get_template_part('partials/landing-page/register-form'); ?>
    		
        <?php endif; ?>
     
    <?php endwhile; endif; ?>


    <div id="overlay">
        <div>
            <div class="overlay-x"><span class="video-x-circle">X</span> CLOSE VIDEO</div>
            <div class="videoWrapper videoWrapperHide">
                <iframe width="1000" height="562" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
            </div>
        </div>
    </div>
    

<?php get_footer(); ?>