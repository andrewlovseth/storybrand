<div class="register-note">
	<p>Have a question? <a href="<?php the_field('have_questions_link', 'options'); ?>" rel="external">Talk to a rep.</a></p>
</div>