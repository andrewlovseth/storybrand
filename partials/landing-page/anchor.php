<section class="anchor">
	<div class="wrapper">
		
		<a href="<?php the_sub_field('link'); ?>" class="btn <?php the_sub_field('color'); ?>"><?php the_sub_field('link_label'); ?></a>

	</div>
</section>