<section class="calendar">
	<div class="wrapper">

		<h3 class="sub-headline"><?php the_sub_field('sub_headline'); ?></h3>
		<h2 class="section-header"><?php the_sub_field('headline'); ?></h2>

		<div class="events">

			<?php if(have_rows('events')): while(have_rows('events')): the_row(); ?>

			    <div class="event cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>);">
			    	<div class="content">
				    	
				    	<div class="location">
				    		<h3><?php the_sub_field('city'); ?></h3>
				    		<h4><?php the_sub_field('state'); ?></h4>
				    		
				    	</div>

				    	<div class="info">
				    		<div class="info-wrapper">

				    			<div class="dates">
									<?php if(have_rows('dates')): while(have_rows('dates')): the_row(); ?>
									 
									    <div class="date">
									        <p><span><?php the_sub_field('date'); ?></span></p>
									    </div>

									<?php endwhile; endif; ?>				    				
				    			</div>

				    			<div class="cta">
				    				<a href="#register" class="btn <?php the_sub_field('color'); ?>"><?php the_sub_field('cta_link_label'); ?></a>
				    			</div>
				    			
				    		</div>
				    	</div>				    		

			    	</div>				        
			    </div>

			<?php endwhile; endif; ?>

		</div>

	</div>
</section>