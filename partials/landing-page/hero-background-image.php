	<section class="hero-background-image cover" style="background-image: url(<?php $image = get_sub_field('background_image'); echo $image['url']; ?>);">

		<div class="content">
			<div class="wrapper">

				<div class="info">
					<h1><?php the_sub_field('headline'); ?></h1>
					<h2><?php the_sub_field('sub_headline'); ?></h2>
				</div>			

			</div>
		</div>
	</section>