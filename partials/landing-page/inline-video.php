<section class="inline-video">
	<div class="wrapper">		

		<div class="poster">
			<a href="#" class="video-trigger" data-src="//player.vimeo.com/video/<?php the_sub_field('vimeo_id'); ?>?title=0&byline=0&portrait=0&autoplay=true">
				<span class="play">
					<span class="label">Watch Video</span>
				</span>				
				<img src="<?php $image = get_sub_field('poster'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>				
		</div>

	</div>
</section>