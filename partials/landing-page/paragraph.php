<section class="paragraph">
	<div class="wrapper">

		<div class="info">
			<?php if(get_sub_field('sub_headline')): ?>
				<h3><?php the_sub_field('sub_headline'); ?></h3>
			<?php endif; ?>

			<h2 class="section-header"><?php the_sub_field('headline'); ?></h2>
			
			<?php the_sub_field('copy'); ?>		
		</div>		

	</div>
</section>