<section class="pricing">
	<div class="wrapper">
		
		<?php $color = get_sub_field('color'); ?>

		<div class="options">
			<?php if(have_rows('pricing_options')): while(have_rows('pricing_options')): the_row(); ?>
		 
		    <div class="option <?php echo $color; ?>">
		    	<div class="header">
		    		<h4><?php the_sub_field('sub_headline'); ?></h4>
		    		<h3><?php the_sub_field('headline'); ?></h3>
		    	</div>

		    	<div class="cost">
		    		<h2><?php the_sub_field('cost'); ?></h2>
		    		<em><?php the_sub_field('cost_note'); ?></em>
		    	</div>

		    	<div class="cta">
		    		<a class="btn <?php echo $color; ?>" href="#register">Register Now</a>
		    	</div>

		    </div>

			<?php endwhile; endif; ?>

		</div>

		<div class="note">
			<?php the_sub_field('note'); ?>
		</div>

	</div>
</section>
