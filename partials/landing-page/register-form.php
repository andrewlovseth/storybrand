<section id="register" class="register-form">
	<div class="wrapper">

		<div class="form-wrapper">
			
			<div class="form-headline">
				<h2 class="section-header"><?php the_sub_field('headline'); ?></h2>
			</div>

			<div class="form-body">
				<?php $shortcode = get_sub_field('shortcode'); echo do_shortcode($shortcode); ?>
			</div>

		</div>
		
	</div>
</section>