<section class="testimonials">

	<?php if(have_rows('testimonials')): while(have_rows('testimonials')): the_row(); ?>
	 
		<div class="slide cover" id="<?php echo sanitize_title_with_dashes(get_sub_field('source')); ?>" style="background-image: url(<?php $image = get_sub_field('background_image'); echo $image['url']; ?>);">
			<div class="content">
				<div class="wrapper">
					
				    <div class="info">
						<blockquote>
							<p><?php the_sub_field('quote'); ?></p>
						</blockquote>

						<cite>
							<h4><?php the_sub_field('source'); ?></h4>
							<h5><?php the_sub_field('source_note'); ?></h5>
						</cite>
				    </div>

				</div>
			</div>
		</div>

	<?php endwhile; endif; ?>

</section>

<section class="testimonials-nav">
	
	<?php if(have_rows('testimonials')): $counter = 1; ?>

		<?php while(have_rows('testimonials')): the_row(); ?>

			<a href="#" data-slide="<?php echo $counter; ?>">
				<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>

		<?php $counter++; endwhile; ?>

	<?php endif; ?>

</section>